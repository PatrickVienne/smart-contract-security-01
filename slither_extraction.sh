FILE=$1
slither ${FILE} --ignore-compile --filter-paths "node_modules" --disable-color --print human-summary > slither-audit.txt 2>&1

# AZURE
# echo "##vso[task.setvariable variable=slitherAuditRun]yes";

export LError=$(grep 'low issues' slither-audit.txt | sed 's/[a-zA-Z:0 ]//g')
export MError=$(grep 'medium issues' slither-audit.txt | sed 's/[a-zA-Z:0 ]//g')
export HError=$(grep 'high issues' slither-audit.txt | sed 's/[a-zA-Z:0 ]//g')

echo -en '-------------------------------\n\n\n# Details:\n##############################\n\n' > slither-audit-full.txt
slither ${FILE} --ignore-compile --filter-paths "node_modules" --disable-color > slither-audit-full.txt 2>&1

LError=${LError:-"0"};
MError=${MError:-"0"};
HError=${HError:-"0"};

if [ ! -z "${LError}" ] || [ ! -z "${MError}" ] || [ ! -z "${HError}" ] ; then
  # AZURE
  # echo "##vso[task.complete result=Failed;]Slither found a issue." ;
  # Gitlab job fail
  echo "Found ${LError} LOW ${MError} MIDDLE ${HError} HIGH Issues"
  echo 1
else
  # AZURE
  # echo "##vso[task.setvariable variable=slitherAudit]$(cat slither-audit.txt | sed ':a;N;$!ba;s/\n/%0D%0A/g')"
  # Gitlab job success
  echo 0
fi
